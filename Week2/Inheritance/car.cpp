#include "car.h"
using namespace week02;

Car::Car(int numberOfPassengers,
         int topSpeed,
         double kilometresPerLitre,
         int numberOfAirBags,
         bool abs,
         int numberOfWheels)
         : MotorVehicle(numberOfPassengers, topSpeed, numberOfWheels, kilometresPerLitre)
         , m_abs(abs)
         , m_numberOfAirBags(numberOfAirBags) {}

Car::Car(int numberOfPassengers,
         int topSpeed,
         double kilometresPerLitre,
         std::string color,
         int numberOfAirBags,
         bool abs,
         int numberOfWheels)
         : MotorVehicle(numberOfPassengers, topSpeed, numberOfWheels, color, kilometresPerLitre)
         , m_abs(abs)
         , m_numberOfAirBags(numberOfAirBags) {}

Car::~Car() {}

int Car::getSafetyRating() {
    int safetyRating = 0;
       if (m_numberOfAirBags >= 4) {
           safetyRating += 3;
       } else if (m_numberOfAirBags >= 2) {
           safetyRating += 2;
       } else if (m_numberOfAirBags > 0) {
           safetyRating += 1;
       }

       if (m_abs) {
           safetyRating += 2;
       }
       return safetyRating;
}
