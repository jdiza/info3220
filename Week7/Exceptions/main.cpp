#include <iostream>

class Exception {
public:
    Exception(std::string error) {
        std::cout << "Exception caught" << std::endl;
    }
};

int main() {
 char c;
 try {
  std::cout << "Type-in (y/n)" << std::endl;
  std::cin >> c;
  if (c != 'y' && c!= 'n') {
   throw Exception("Wrong input character");
  }
 } catch(Exception e) {
  std::cout << "Wrong input\n";
  exit(1);
 }
 if (c=='y') {
  std::cout << "User typed yes." << std::endl;
 } else {
  std::cout << "User typed no." << std::endl;
 }
 return 0;
}
