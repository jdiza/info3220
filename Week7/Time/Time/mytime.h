#ifndef MYTIME_H
#define MYTIME_H

#include <string>

namespace Week07 {
class MyTime
{
public:
    MyTime(int hours, int minutes, int seconds);

    ~MyTime();

    double numberOfHours() const;
    double numberOfMinutes() const;
    int numberOfSeconds() const;

    std::string getTimeAsString() const;

private:
    MyTime();
    int m_hours;
    int m_minutes;
    int m_seconds;
};
}
#endif
