#include<iostream>

class AbstractIDE {  // Product (abstract)
public:
   AbstractIDE() {}
   virtual ~AbstractIDE() {}
   virtual int getCost() = 0; // pure virtual method
};
class WindowsIDE : public AbstractIDE { // Concrete Product
public:
   int getCost() override {
      return 4500;
   }
};

class MacIDE : public AbstractIDE { // Concrete Product
public:
   int getCost() override {
      return 99;
   }
};

class AbstractMalware {  // Product (abstract)
public:
   AbstractMalware() {}
   virtual ~AbstractMalware() {}
   virtual int getFrequency() = 0; // pure virtual method
};

class WindowsMalware : public AbstractMalware { // Concrete Product
public:
   int getFrequency() override {
      return 0;
   }
};

class MacMalware : public AbstractMalware { // Concrete Product
public:
   int getFrequency() override {
      return 1;
   }
};
class AbstractOS { // Factory (abstract)
public:
   AbstractOS() {}
   virtual ~AbstractOS() { }
   virtual AbstractIDE *makeIDE() = 0; // pure virtual
   virtual AbstractMalware *makeMalware() = 0; // pure virtual
};
class WindowsOS : public AbstractOS {
public:
    AbstractIDE *makeIDE() {
        AbstractIDE *windows = new WindowsIDE();
        ide = windows;
        return windows;
    }
    AbstractMalware *makeMalware() {
        AbstractMalware *windowsmware = new WindowsMalware();
        malware = windowsmware;
        return windowsmware;
    }
    virtual ~WindowsOS() {
        delete ide;
        delete malware;
    }
private:
    AbstractIDE *ide;
    AbstractMalware *malware;
};
class MacOS : public AbstractOS {
public:
    AbstractIDE *makeIDE() {
        AbstractIDE *mac = new MacIDE();
        ide = mac;
        return mac;
    }
    AbstractMalware *makeMalware() {
        AbstractMalware *macmware = new MacMalware();
        malware = macmware;
        return macmware;
    }
    virtual ~MacOS() {
        delete ide;
        delete malware;
    }
private:
    AbstractIDE *ide;
    AbstractMalware *malware;
};

int main() {
    AbstractOS *windows = new WindowsOS();
    AbstractOS *mac = new MacOS();
    std::cout << mac->makeIDE()->getCost() <<std::endl;
    std::cout << mac->makeMalware()->getFrequency() <<std::endl;
    std::cout << windows->makeIDE()->getCost() <<std::endl;
    std::cout << windows->makeMalware()->getFrequency() <<std::endl;
    delete windows;
    delete mac;
    return 0;
}
