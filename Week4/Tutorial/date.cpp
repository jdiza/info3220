#include "date.h"
using namespace Week4;

Date::Date(int day, int month, int year)
    :m_day(day)
    ,m_month(month)
    ,m_year(year) {
}

Date::~Date() {}

int Date::getYear() const {
    return m_year;
}

int Date::getMonth() const {
    return m_month;
}

int Date::getDay() const {
    return m_day;
}

std::string Date::dateAsString() const {
    std::string day = std::to_string(m_day);
    std::string month = std::to_string(m_month);
    std::string year = std::to_string(m_year);

    std::string output = day + "/" + month + "/" + year;

    return output;
}
