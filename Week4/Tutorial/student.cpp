#include "student.h"

using namespace Week4;
Student::Student(const std::string &givenName, const std::string &surname,
                 const Date &date, const std::string &degree)
    :Person(givenName, surname, date) {
    m_degree = new std::string(degree);
    m_recordAccessed = 0;
}

Student::~Student() {
    delete m_degree;
}

std::string Student::getRecord() const {
    std::string firstLine = "Name: " + getFirstName() + " " + getSurname();
    std::string secondLine = "Date Of Birth: " + m_dateOfBirth.dateAsString();
    std::string thirdLine = *m_degree;

    std::stringstream output;
    output << firstLine << std::endl << secondLine << std::endl << thirdLine;

    return output.str();
}
