#ifndef STAGE3POOLGAME_H
#define STAGE3POOLGAME_H
#include "poolgame.h"
#include "pooloriginator.h"
#include "mementcaretaker.h"
#include "dialog.h"
#include <QWidget>
#include <QKeyEvent>
#include "stage2ball.h"

class Stage3PoolGame : public PoolGame, public QObject {

public:
    Stage3PoolGame(Table *table, std::vector<Ball*> balls, Dialog * parent);
    virtual ~Stage3PoolGame();
    virtual void simulateTimeStep(float timeStep);
public slots:
    void revertToSaved(QKeyEvent *event);
    void mousePressed(QMouseEvent * event);
protected:
    PoolOriginator *m_gameOriginator;
    MementCaretaker *m_gameCaretaker;

};

#endif // STAGE3POOLGAME_H
