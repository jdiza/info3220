#ifndef POOLMEMENTO_H
#define POOLMEMENTO_H
#pragma once
#include "ball.h"
#include "table.h"


class PoolMemento
{
    friend class PoolOriginator;
public:
    virtual ~PoolMemento();
    PoolMemento(std::vector<Ball*> balls, Table * table);
    std::vector<Ball*> getBalls();
    Table* getTable();
private:
    std::vector<Ball*> m_gameBalls;
    Table * m_gameTable;
};

#endif // POOLMEMENTO_H
