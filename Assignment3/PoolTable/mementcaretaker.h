#ifndef MEMENTCARETAKER_H
#define MEMENTCARETAKER_H
#include "poolmemento.h"
#include <QList>

class MementCaretaker
{
public:
    MementCaretaker();
    void addSave(PoolMemento* gameToSave);
    PoolMemento* getLastSave();

private:
    QList<PoolMemento*> m_savedGames;
};

#endif // MEMENTCARETAKER_H
