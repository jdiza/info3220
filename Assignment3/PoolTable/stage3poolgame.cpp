#include "stage3poolgame.h"
#include <iostream>

Stage3PoolGame::Stage3PoolGame(Table *table, std::vector<Ball*> balls, Dialog *parent)
    :PoolGame(table, balls) {
    m_gameOriginator = new PoolOriginator(m_balls,m_table);
    m_gameCaretaker = new MementCaretaker();
    PoolMemento *configSave = m_gameOriginator->saveGame();
    m_gameCaretaker->addSave(configSave);
    connect(parent,&Dialog::mousePressed,this,&Stage3PoolGame::mousePressed);
    connect(parent,&Dialog::keyPressed,this,&Stage3PoolGame::revertToSaved);
}

Stage3PoolGame::~Stage3PoolGame() {
    delete m_gameOriginator;
    delete m_gameCaretaker;
}

void Stage3PoolGame::revertToSaved(QKeyEvent *event) {
    if( event->key() == Qt::Key_U ) {
        std::cout << "Undo Pressed" << std::endl;
        m_gameOriginator->restoreFromMemento(m_gameCaretaker->getLastSave());
        m_balls = m_gameOriginator->getBalls();
        m_table = m_gameOriginator->getTable();

    }
}

void Stage3PoolGame::simulateTimeStep(float timeStep)
{
    ChangeInPoolGame totalChange;
    //collisions of balls with the edge of the table
    for(Ball * b: m_balls)
    {
        totalChange = totalChange.merge(m_table->ballCollision(b));
    }
    //a collision between each possible pair of balls
    for(size_t i = 0; i < m_balls.size();++i)
    {
        for(size_t j = i+1;j < m_balls.size();++j)
        {
            if(m_balls[i]->collidesWith(m_balls[j]))
            {
                totalChange = totalChange.merge(collide(m_balls[i],m_balls[j]));
            }
        }
    }

    for(Ball * e:totalChange.m_ballsToRemove)
    {
        m_balls.erase(std::find(m_balls.begin(),m_balls.end(),e));
    }

    for(Ball * a:totalChange.m_ballsToAdd)
    {
        m_balls.push_back(a);
    }
    for(Ball * b: m_balls)
    {
        b->move(timeStep);

        //apply friction
        b->changeVelocity(-m_table->friction()*timeStep*b->velocity());

        //if moving less than 5 pixels per second just stop
        if(b->velocity().length()<5) {
            b->setVelocity(QVector2D(0,0));
        }
    }
}

void Stage3PoolGame::mousePressed(QMouseEvent *event) {
    bool foundCueBall = false;

    for(Ball *b:m_balls) {
        if(foundCueBall == false && b->colour()==QColor("white")) {
            if(b->velocity().lengthSquared()<0.001 && (QVector2D(event->pos())-b->position()).length() < b->radius()) {
                PoolMemento *configSave = m_gameOriginator->saveGame();
                m_gameCaretaker->addSave(configSave);;
                foundCueBall = true;
            }
        }
    }
}
