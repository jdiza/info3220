#include "stage3ball.h"

#include <cmath>

void Stage3Ball::draw(QPainter &p)
{
    p.setPen(Qt::black);
    p.setBrush(QBrush(m_colour));
    p.drawEllipse(m_position.toPointF(),m_radius,m_radius);
}

Stage3CompositeBall::~Stage3CompositeBall()
{
    for(Ball * b: m_containedBalls)
    {
        delete b;
    }
}

ChangeInPoolGame Stage3CompositeBall::changeVelocity(const QVector2D &deltaV)
{
    float energyOfCollision = mass()*deltaV.lengthSquared();
    if(energyOfCollision>m_strength)
    {
        float energyPerBall = energyOfCollision/m_containedBalls.size();
        QVector2D pointOfCollision((-deltaV.normalized())*m_radius);
        //for each component ball
        for(Ball * b: m_containedBalls)
        {
            b->setVelocity(m_velocity + sqrt(energyPerBall/b->mass())*(b->position()-pointOfCollision).normalized());
            b->setPosition(m_position+b->position());
        }
        ChangeInPoolGame change({this},std::move(m_containedBalls));
        m_containedBalls.clear();
        return change;
    }
    m_velocity += deltaV;
    return ChangeInPoolGame();
}

float Stage3CompositeBall::mass() const
{
    return m_mass+m_containedMass;
}

void Stage3CompositeBall::draw(QPainter &p)
{
    Stage3Ball::draw(p);

    if(drawChildren)
    {
        //recursively draw children, translating the painter
        //so that the children are drawn realative to the parent
        p.translate(m_position.toPointF());
        for(Ball * b: m_containedBalls)
        {
            b->draw(p);
        }
        p.translate(-m_position.toPointF());
    }

}

void Stage3CompositeBall::setRadius(float newRadius)
{
    if(m_radius==-1)
        m_radius = newRadius;
    else
    {
        float ratio = newRadius/radius();
        m_radius = newRadius;
        //recursively decrease the radius of children
        for(Ball * b: m_containedBalls)
        {
            b->setRadius(b->radius()*ratio);
            b->setPosition(b->position()*ratio);
        }
    }
}

ChangeInPoolGame SimpleStage3Ball::changeVelocity(const QVector2D &deltaV)
{
    if(mass()*deltaV.lengthSquared()>m_strength)
        return ChangeInPoolGame({this});
    m_velocity += deltaV;
    return ChangeInPoolGame();
}

std::vector<Ball*> Stage3CompositeBall::getContainedBalls() {
    return m_containedBalls;
}

Ball* SimpleStage3Ball::clone() {
    Ball* returnBall;
    SimpleStage3Ball* clonedBall = new SimpleStage3Ball();
    clonedBall->m_colour = this->m_colour;
    clonedBall->m_mass = this->m_mass;
    clonedBall->m_position = this->m_position;
    clonedBall->m_radius = this->m_radius;
    clonedBall->m_strength = this->m_strength;
    clonedBall->m_velocity = this->m_velocity;
    returnBall = clonedBall;
    return returnBall;
}

Ball* Stage3CompositeBall::clone() {

    Ball* returnBall;
    Stage3CompositeBall* clonedBall = new Stage3CompositeBall();
    clonedBall->m_colour = this->m_colour;
    clonedBall->m_mass = this->m_mass;
    clonedBall->m_position = this->m_position;
    clonedBall->m_radius = this->m_radius;
    clonedBall->m_strength = this->m_strength;
    clonedBall->m_velocity = this->m_velocity;
    if(this->m_containedBalls.empty() == false) {
        for(Ball* childBall: this->m_containedBalls) {
            clonedBall->addBall(childBall->clone());
        }
    }
    returnBall = clonedBall;
    return returnBall;
}
