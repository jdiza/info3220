#include "mementcaretaker.h"

MementCaretaker::MementCaretaker() {

}
void MementCaretaker::addSave(PoolMemento* gameToSave) {
    m_savedGames.push_front(gameToSave);
    if(m_savedGames.size() > 1) {
        delete m_savedGames.at(1);
    }

}
PoolMemento* MementCaretaker::getLastSave() {
    return m_savedGames.first();
}
