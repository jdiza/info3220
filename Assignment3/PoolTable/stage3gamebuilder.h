#ifndef STAGE3GAMEBUILDER_H
#define STAGE3GAMEBUILDER_H
#include "gamebuilder.h"
#include "stage3poolgame.h"

class Stage3GameBuilder : public GameBuilder
{
public:
    Stage3GameBuilder(AbstractFactory *factory, Dialog *parent);
    virtual ~Stage3GameBuilder();
    virtual PoolGame* getGame();

private:
    Dialog* m_dialog;
};

#endif // STAGE3GAMEBUILDER_H
