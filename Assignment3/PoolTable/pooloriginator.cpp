#include "pooloriginator.h"

PoolOriginator::PoolOriginator(std::vector<Ball*>balls, Table* table)
    :m_currentBalls(balls), m_currentTable(table)
{

}

PoolMemento* PoolOriginator::saveGame() {
    std::vector<Ball*> deepCopiedBalls = cloneBalls(m_currentBalls);
    Table * deepCopiedTable = cloneTable(m_currentTable);
    return new PoolMemento(deepCopiedBalls, deepCopiedTable);
}

void PoolOriginator::restoreFromMemento(PoolMemento *memento) {
    m_currentBalls = cloneBalls(memento->getBalls());
    m_currentTable = cloneTable(memento->getTable());
}

std::vector<Ball*> PoolOriginator::cloneBalls(std::vector<Ball*> balls) {
    std::vector<Ball*> deepCopiedBalls;
    for(Ball* currentBall: balls) {
        Ball* newClonedBall = currentBall->clone();
        deepCopiedBalls.push_back(newClonedBall);
    }
    return deepCopiedBalls;
}

Table* PoolOriginator::cloneTable(Table* table) {
    return table->clone();
}
