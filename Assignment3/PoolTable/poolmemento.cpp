#include "poolmemento.h"

PoolMemento::PoolMemento(std::vector<Ball *> balls, Table *table)
    :m_gameBalls(balls)
    ,m_gameTable(table)
{


}

std::vector<Ball*> PoolMemento::getBalls() {
    return this->m_gameBalls;
}
Table* PoolMemento::getTable() {
    return this->m_gameTable;
}


PoolMemento::~PoolMemento() {
    delete m_gameTable;
    for(Ball * b: m_gameBalls)
    {
        delete b;
    }
}
