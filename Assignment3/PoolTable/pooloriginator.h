#ifndef POOLORIGINATOR_H
#define POOLORIGINATOR_H
#pragma once
#include "poolmemento.h"
#include "ball.h"
#include "table.h"
#include "poolgame.h"

class PoolOriginator
{
public:
    PoolOriginator(std::vector<Ball *> balls, Table *table);
    PoolMemento* saveGame();
    void restoreFromMemento(PoolMemento* memento);
    std::vector<Ball*> getBalls() {return m_currentBalls;}
    Table* getTable() {return m_currentTable;}

private:
    Table* cloneTable(Table *table);
    std::vector<Ball*> cloneBalls(std::vector<Ball*> balls);
    std::vector<Ball*> m_currentBalls;
    Table* m_currentTable;
};

#endif // POOLORIGINATOR_H
