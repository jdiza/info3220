#include "stage3gamebuilder.h"

Stage3GameBuilder::Stage3GameBuilder(AbstractFactory *factory, Dialog *parent)
    :GameBuilder(factory)
    ,m_dialog(parent) {


}
PoolGame* Stage3GameBuilder::getGame() {
    PoolGame * result = new Stage3PoolGame(m_table,std::move(m_balls),m_dialog);
    m_table = nullptr;
    m_balls.clear();
    return result;
}
Stage3GameBuilder::~Stage3GameBuilder()
{
    //in normal operation m_table is likely to be nullptr
    //and m_balls is likely to empty
    //this is only not the case if the director terminates the builder mid build
    delete m_table;
    for(Ball * b: m_balls)
    {
        delete b;
    }
}
