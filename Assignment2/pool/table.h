#ifndef TABLE_H
#define TABLE_H

#include "abstracttable.h"
#include <QList>
#include "pocket.h"
#include <QPen>
/**
 * @brief The Table is a concrete implementation of the abstract table, with nothing changed.
 *
 * @author ??
 * @date April 2018
 *
 * It is a static, drawn object that doesn't move during runtime.
 * It holds the boundaries of the pool game which are used for colision calculations.
 * These parameters should not change during runtime.
 */

class Table : public AbstractTable
{
public:
    Table(float length, float height, float space, float friction, std::string color);
    QList<Pocket*> getPockets();
    void addPocket(Pocket* pocket);
    void render(QPainter &painter);
private:
    QList<Pocket*> m_pockets;
};
#endif // TABLE_H
