#include "table.h"


/**
 * @brief The Table is a concrete implementation of the abstract table, with nothing changed.
 *
 * @author ??
 * @date April 2018
 *
 * It is a static, drawn object that doesn't move during runtime.
 * It holds the boundaries of the pool game which are used for colision calculations.
 * These parameters should not change during runtime.
 */

Table::Table(float length, float height, float space, float friction, std::string color)
    : AbstractTable(length, height, space, friction, color)
{}

QList<Pocket*> Table::getPockets() {
    return m_pockets;
}

void Table::addPocket(Pocket * pocket) {
    m_pockets.append(pocket);
}

void Table::render(QPainter &painter){
    QPen pen;
    pen.setWidth(2);
    pen.setColor(QColor(QString::fromStdString("brown")));
    painter.setPen(pen);

    painter.setBrush(QBrush(QColor(QString::fromStdString(this->getColour()))));
    painter.drawRect(QRect(this->getSpace(), this->getSpace(), this->getLength(), this->getHeight()));
    painter.setBrush(QBrush(QColor("black")));
    for(Pocket *currentPocket : this->m_pockets) {
        currentPocket->render(painter);
        //painter.drawEllipse(QPoint(int(currentPocket->getCoordinate().getQtRenderingXCoordinate()), int(currentPocket->getCoordinate().getQtRenderingYCoordinate())), (int)currentPocket->getRadius(), (int)currentPocket->getRadius());
    }
}
