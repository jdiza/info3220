#include "cuestick.h"

CueStick::CueStick() {
}

void CueStick::render(QPainter &painter) {
    //painter.setPen();
    painter.setBrush(QBrush(QColor(QString::fromStdString("red"))));
    painter.drawLine(m_origin, m_currentPos);
    //painter.drawLine(m_origin, m_currentPos);
}
QPoint CueStick::getCurrentPos() {
    return m_currentPos;
}
QPoint CueStick::getOrigin() {
    return m_origin;
}
void CueStick::setOrigin(QPoint origin) {
    m_origin = origin;
}
void CueStick::setCurrent(QPoint current) {
    m_currentPos = current;
}
float CueStick::getxVelocity() {
    m_xVelocity = m_origin.rx() - m_currentPos.rx();
    return m_xVelocity;
}

float CueStick::getyVelocity() {
    m_yVelocity = m_origin.ry() - m_currentPos.ry();
    return m_yVelocity;
}

void CueStick::setxVelocity(float xValue) {
    m_xVelocity = xValue;
}
void CueStick::setyVelocity(float yValue) {
    m_yVelocity = yValue;
}
void CueStick::reset() {
    this->setOrigin(QPoint(0,0));
    this->setCurrent(QPoint(0,0));
}
