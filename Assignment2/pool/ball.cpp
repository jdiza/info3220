#include "ball.h"

#include <cmath>
#include <string>

/**
 * @brief The ball class is an implementation of the abstract ball class. Nothing has changed.
 *
 * @author ??
 * @date April 2018
 *
 * Each ball will have a given coordinate, velocity, mass, radius and colour.
 * Balls will render themselves.
 */

Ball::Ball(){}

Ball::Ball(Coordinate coordinate, std::string color, float mass, float radius, float xVelocity, float yVelocity, float strength)
    : AbstractBall(coordinate, color, mass, radius, xVelocity, yVelocity)
    ,m_strength(strength)
{}

void Ball::render(QPainter &painter){
    painter.setPen(Qt::black);
    painter.setBrush(QBrush(QColor(QString::fromStdString(this->getColor()))));
    painter.drawEllipse(QPoint(int(this->getCoordinate().getQtRenderingXCoordinate()),int(this->getCoordinate().getQtRenderingYCoordinate())), (int)this->getRadius(), (int)this->getRadius());
}

float Ball::getStrength() {
    return m_strength;
}

QList<Ball *> Ball::getInnerBalls() {
    return m_innerBalls;
}
void Ball::stopMovement() {
    m_xVelocity = 0;
    m_yVelocity = 0;
}
void Ball::changeXVelocity(float change){
    m_xVelocity += change;
}

void Ball::changeYVelocity(float change){
    m_yVelocity += change;
}
void Ball::changeinXCoordinate(float value){
    m_coordinate.changeInXCoordinate(value);
}
void Ball::changeinYCoordinate(float value){
    m_coordinate.changeInYCoordinate(value);
}
void Ball::addInnerBall(Ball *newBall) {
    m_innerBalls.append(newBall);
}
