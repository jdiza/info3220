#ifndef ABSTRACTBALL_H
#define ABSTRACTBALL_H

#include "coordinate.h"
#include <QPainter>

/**
 * @brief An abstract ball class.
 *
 * @author ??
 * @date April 2018
 *
 * Each ball will have a given coordinate, velocity, mass, radius and colour.
 * Balls will render themselves.
 */


class AbstractBall
{
public:
    /**
     * @brief AbstractBall
     * Default Constructor in order for balls to be passed as pointers
     */
    AbstractBall(){
        m_coordinate = Coordinate(0,0,0,0);
        m_color = new std::string("white");
        m_mass = 1;
        m_radius = 10;
        m_xVelocity = 0;
        m_yVelocity = 0;
    }
    /**
     * @brief AbstractBall
     * Constructor to be used when all vbariables are known. Director automatically passes default values to contructor.
     * @param coordinate
     * Coordinate class to control ball location
     */
    AbstractBall(Coordinate coordinate, std::string color, float mass, float radius, float xVelocity, float yVelocity)
        : m_coordinate(coordinate)
        , m_color(new std::string(color))
        , m_mass(mass)
        , m_radius(radius)
        , m_xVelocity(xVelocity)
        , m_yVelocity(yVelocity)
    {}

    // Destructor. Must delete the dynamically allocated color string
    virtual ~AbstractBall(){
        delete m_color;
    }

    /**
     * @brief render - Each ball will render itself. This draws a at the given (x,y) coordinate in the given colour
     * @param painter
     */
    /*void render(QPainter &painter){
        painter.setPen(Qt::black);
        painter.setBrush(QBrush(QColor(QString::fromStdString(*m_color))));
        painter.drawEllipse(QPoint(int(m_coordinate.getQtRenderingXCoordinate()),int(m_coordinate.getQtRenderingYCoordinate())), (int)m_radius, (int)m_radius);
    }*/
    virtual void render(QPainter &painter) = 0;
    // Get methods
    virtual Coordinate getCoordinate(){
        return m_coordinate;
    }

    virtual float getRadius(){
        return m_radius;
    }

    virtual float getMass(){
        return m_mass;
    }

    virtual std::string getColor(){
        return *m_color;
    }

    virtual float getXVelocity(){
        return m_xVelocity;
    }

    virtual float getYVelocity(){
        return m_yVelocity;
    }

    // Set methods
    virtual void changeinXCoordinate(float value){
        m_coordinate.changeInXCoordinate(value);
    }
    virtual void changeinYCoordinate(float value){
        m_coordinate.changeInYCoordinate(value);
    }
    /**
     * @brief flipXVelocity changes direction of Ball on the X Axis
     */
    virtual void flipXVelocity(){
        m_xVelocity = m_xVelocity * -1.0;
    }
    /**
     * @brief flipYVelocity changes direction of Ball on the Y Axis
     */
    virtual void flipYVelocity(){
        m_yVelocity = m_yVelocity * -1.0;
    }

    virtual void changeXVelocity(float change){
        m_xVelocity += change;
    }

    virtual void changeYVelocity(float change){
        m_yVelocity += change;
    }
    /**
     * @brief stopMovement Used to stop any movement.
     */
    virtual void stopMovement() {
        m_xVelocity = 0;
        m_yVelocity = 0;
    }


protected:
    Coordinate m_coordinate;
    std::string* m_color;
    float m_mass;
    float m_radius;
    float m_xVelocity;
    float m_yVelocity;
};

#endif // ABSTRACTBALL_H
