#ifndef CUESTICK_H
#define CUESTICK_H
#include <QPoint>
#include <QPainter>

class CueStick
{
public:
    CueStick();
    void render(QPainter &painter);
    /**
     * @brief getOrigin
     * @return The position of the cueball.
     */
    QPoint getOrigin();
    /**
     * @brief getCurrentPos
     * @return The positon of the mouse cursor
     */
    QPoint getCurrentPos();
    /**
     * @brief getxVelocity
     * @return Difference between cue ball position and mouse position.
     */
    float getxVelocity();
    /**
     * @brief getyVelocity
     * @return Difference between cue ball position and mouse positoin.
     */
    float getyVelocity();
    void setxVelocity(float xValue);
    void setyVelocity(float yValue);
    void setOrigin(QPoint origin);
    void setCurrent(QPoint current);
    /**
     * @brief reset Sets the position of the cuestick to 0,0
     */
    void reset();

private:
    QPoint m_origin;
    QPoint m_currentPos;
    float m_xVelocity;
    float m_yVelocity;
};

#endif // CUESTICK_H
