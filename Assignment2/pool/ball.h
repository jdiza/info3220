#ifndef BALL_H
#define BALL_H

#include "coordinate.h"
#include <QPainter>
#include "abstractball.h"

/**
 * @brief The ball class is an implementation of the abstract ball class. Nothing has changed.
 *
 * @author ??
 * @date April 2018
 *
 * Each ball will have a given coordinate, velocity, mass, radius and colour.
 * Balls will render themselves.
 */


class Ball : public AbstractBall
{

public:
    /**
     * @brief Ball Default constructor so balls can be passed for wrapper classes.
     */
    Ball();
    Ball(Coordinate coordinate, std::string color, float mass, float radius, float xVelocity, float yVelocity, float strength);
    virtual void render(QPainter &painter);
    virtual float getStrength();
    /**
     * @brief getInnerBalls
     * @return list of balls contained inside
     */
    virtual QList<Ball *> getInnerBalls();
    virtual void changeinXCoordinate(float value);
    virtual void changeinYCoordinate(float value);
    virtual void changeXVelocity(float change);
    virtual void changeYVelocity(float change);
    virtual void stopMovement();
    /**
     * @brief addInnerBall Function to add a ball inside this ball.
     * @param newBall
     */
    virtual void addInnerBall(Ball* newBall);

private:
    float m_strength;
    QList<Ball *> m_innerBalls;
};
#endif // BALL_H
