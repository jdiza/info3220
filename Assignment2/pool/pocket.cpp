#include "pocket.h"

Pocket::Pocket(Coordinate coordinate, float radius)
    :m_coordinate(coordinate)
    ,m_radius(radius)
{

}
Coordinate Pocket::getCoordinate() {
    return m_coordinate;
}
float Pocket::getRadius() {
    return m_radius;
}

void Pocket::render(QPainter &painter) {
    painter.setPen(Qt::black);
    painter.setBrush(Qt::black);
    painter.drawEllipse(QPoint(int(m_coordinate.getQtRenderingXCoordinate()),int(m_coordinate.getQtRenderingYCoordinate())), (int)m_radius, (int)m_radius);
}
