#ifndef POCKET_H
#define POCKET_H
#include "coordinate.h"
#include <QPainter>

class Pocket
{
public:
    Pocket(Coordinate coordinate, float radius);
    Coordinate getCoordinate();
    float getRadius();
    void render(QPainter &painter);

private:
    Coordinate m_coordinate;
    float m_radius;
};

#endif // POCKET_H
