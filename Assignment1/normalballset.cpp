#include "normalballset.h"
#include <iostream>
#include <stdlib.h>

void NormalBallSet::render(QPainter &painter) {
    bool whiteBallLimitReached = false;
    for(Ball *ball : this->getPlayingBalls()) {
        QColor ballColour = ball->getColour();

        if(ballColour == QColor("white")) {
            if(whiteBallLimitReached != true) {
                whiteBallLimitReached = true;
            }
            else {
                std::cout << "Multiple white balls detected" << std::endl;
                exit(1);
            }
        }
        painter.setBrush(QBrush(ballColour));
        painter.drawEllipse(0,0, ball->getRadius(), ball->getRadius());
    }
}

void NormalBallSet::moveBalls(float friction) {
    for(Ball *ball : this->getPlayingBalls()) {
        ball->move(friction);
    }
}




