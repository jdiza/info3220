#ifndef NORMALTABLE_H
#define NORMALTABLE_H

#include "abstracttable.h"
#include <QPainter>
#include <QBrush>
#include <QRect>

class NormalTable : public AbstractTable {

public:
    /**
     * @brief Draws rectangle to represent the table.
     */
    void render(QPainter &painter);

    /**
     * @brief Sets the table to appear as a QRect with top left corner at 0,0.
     */
    void createTableImage();

private:
    /**
     * @brief tableImage as a QRect but can be changed to a different type of representation in the future.
     */
    QRect tableImage;
};

#endif // NORMALTABLE_H
