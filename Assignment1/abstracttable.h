#ifndef ABSTRACTTABLE_H
#define ABSTRACTTABLE_H

#include <QString>
#include <QColor>
#include <QPainter>

class AbstractTable {
public:
    AbstractTable(){}
    virtual ~AbstractTable() {}

    QColor getColour() const{
        return m_colour;
    }

    int getXSize() const{
        return m_xSize;
    }

    int getYSize() const{
        return m_ySize;
    }

    float getFriction() const{
        return m_friction;
    }

    /**
     * @brief Takes in string specified from config file and converts to QColor
     *
     */
    void setColour(QString colour) {
        m_colour = QColor(colour);
    }

    void setXSize(int x) {
        m_xSize = x;
    }

    void setYSize(int y) {
        m_ySize = y;
    }

    void setFriction(float friction) {
        m_friction = friction;
    }

    /**
     * @brief Pure virtual function to allow different types of tables to render differently.
     *
     */
    virtual void render(QPainter &painter) = 0;

    /**
     * @brief Pure virtual function to allow different types of tables to have different visual representations.
     *
     */
    virtual void createTableImage() = 0;

private:
    QColor m_colour;
    int m_xSize;
    int m_ySize;
    float m_friction;
};

#endif // ABSTRACTTABLE_H
