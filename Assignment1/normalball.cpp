#include "normalball.h"

void NormalBall::move(float friction) {
    double speedX = this->getVelocity().x() * 0.032;
    double speedY = this->getVelocity().y() * 0.032;
    int newXPos = this->getPosition().x() + round(speedX);
    int newYPos = this->getPosition().y() + round(speedY);
    this->setPosition(QPoint(newXPos, newYPos));
    this->decelerate(friction);
}

void NormalBall::decelerate(float friction) {
    double newVelX = this->getVelocity().x() * (1-friction);
    double newVelY = this->getVelocity().y() * (1-friction);
    this->setVelocity(QVector2D(newVelX, newVelY));
}

