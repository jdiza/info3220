#include "normalballbuilder.h"

NormalBall* NormalBallBuilder::buildBall(const QJsonValue &ballSpecs) {
    NormalBall *newBall = new NormalBall();
    QJsonObject ballVariables = ballSpecs.toObject();    


    for(QString &key : ballVariables.keys()) {
        if(key == "colour") {
            newBall->setColour(ballVariables.value(key).toString());
        }

        else if(key == "position") {
            QJsonValue posObj = ballVariables.value(key);
            QJsonObject posVariables = posObj.toObject();
            int xPos = posVariables.value(QString("x")).toInt();
            int yPos = posVariables.value(QString("y")).toInt();
            newBall->setPosition(QPoint(xPos, yPos));
        }

        else if(key == "velocity") {
            QJsonValue velObj = ballVariables.value(key);
            QJsonObject velVariables = velObj.toObject();
            float xVel = velVariables.value(QString("x")).toDouble();
            float yVel = velVariables.value(QString("y")).toDouble();
            QVector2D velocity(xVel, yVel);
            newBall->setVelocity(velocity);
        }

        else if(key == "mass") {
            newBall->setMass(ballVariables.value(key).toDouble());
        }

        else if(key == "radius") {
            newBall->setRadius(ballVariables.value(key).toInt());
        }

        else {
            std::cout << "Error in keys of balls object in config file" << std::endl;
        }
    }
    return newBall;
}
