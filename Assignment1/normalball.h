#ifndef NORMALBALL_H
#define NORMALBALL_H

#include "ball.h"
#include <iostream>
#include <math.h>

class NormalBall : public Ball {

public: 
    /**
     * @brief Creates speed variables from velocity and converts pixels per second to pixels per frame.
     * NOTE: Hardcoded conversion from pixel/sec to pixel/frame. If timer changes from dialog file, be sure to change the conversion here.
     * Calculates the new position of the ball by adding the pixels/frame to current position and decelerates the ball.
     * @param friction as specified from table object.
     */
    void move(float friction);

private:
    /**
     * @brief Multiplies velocity by 1 - friciton.
     * @param friction ideally should be greater than 0 or less than 1.
     */
    void decelerate(float friction);
};

#endif // NORMALBALL_H
