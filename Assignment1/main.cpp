#include "gameDialog.h"
#include <QApplication>
#include <QIODevice>

//Change config file path here. By default, points to a folder which can contain multiple config files in the source directory.
#define CONFIG_FILE "../Assignment1/configFiles/config.json"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QString const fileName = CONFIG_FILE;
    GameDialog w(fileName);
    w.show();

    return a.exec();
}
