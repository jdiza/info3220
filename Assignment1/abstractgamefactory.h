#ifndef ABSTRACTGAMEFACTORY_H
#define ABSTRACTGAMEFACTORY_H

#include "abstracttable.h"
#include "abstractballset.h"

class AbstractGameFactory {
public:
    AbstractGameFactory() {}
    virtual ~AbstractGameFactory() {}

    /**
     * @brief Interface function to create table product for Concrete Factories.
     * @return Pointer to a Abstract Table.
     */
    virtual AbstractTable* makeTable() = 0;

    /**
     * @brief Interface function to create ball set product for Concrete Factories.
     * @return Pointer to a Abstract Ballset.
     */
    virtual AbstractBallSet* makeBalls() = 0;

};

#endif // ABSTRACTGAMEFACTORY_H
