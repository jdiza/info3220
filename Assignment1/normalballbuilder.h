#ifndef NORMALBALLBUILDER_H
#define NORMALBALLBUILDER_H
#include "normalball.h"

#include <iostream>
#include <QString>
#include <QStringList>
#include <QJsonObject>
#include <QJsonValue>
#include <QPoint>
#include <QVector2D>

class NormalBallBuilder {
public:
    /**
     * @brief Builder function which sets colour, position, velocity, mass and radius of ball.
     * @param ballSpecs used instead of multiple "parts" parameters since the QJsonValue object already holds all aspects of the ball.
     * @return A pointer to a NormalBall object.
     */
    NormalBall* buildBall(const QJsonValue &ballSpecs);
};

#endif // NORMALBALLBUILDER_H
