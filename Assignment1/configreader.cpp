#include "configreader.h"

ConfigReader::ConfigReader(QFile *file) {
    m_file = file;
}

const QJsonObject ConfigReader::readFile() const{
    if(!m_file->open(QIODevice::ReadOnly)) {
        std::cout << "Failed to open " << m_file->fileName().toStdString() << std::endl;
        exit(1);

    }
    QString val;
    val = m_file->readAll();
    QJsonDocument decoder = QJsonDocument::fromJson(val.toUtf8());
    const QJsonObject gameObjects = decoder.object();
    return gameObjects;
}
