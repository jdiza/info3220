#include "normalpoolgame.h"

NormalPoolGame::NormalPoolGame(const QJsonObject gameObjects)
    :m_gameObjects(gameObjects) {}

NormalPoolGame::~NormalPoolGame() {
    delete m_table;
    delete m_ballSet;
}


AbstractTable* NormalPoolGame::makeTable() {
    AbstractTable *newTable = new NormalTable();
    QJsonValue table = m_gameObjects.value(QString("table"));
    QJsonObject tableVariables = table.toObject();

    for(QString &key: tableVariables.keys()) {
        if(key == "colour") {
            newTable->setColour(tableVariables.value(key).toString());
        }
        else if(key == "size") {
            QJsonValue sizeObj = tableVariables.value(key);
            QJsonObject sizeVariables = sizeObj.toObject();
            newTable->setXSize(sizeVariables.value(QString("x")).toInt());
            newTable->setYSize(sizeVariables.value(QString("y")).toInt());
        }
        else if(key == "friction") {
            newTable->setFriction(tableVariables.value(key).toDouble());
        }
        else {
            std::cout << "Error in keys of table object in config file" << std::endl;
        }
    }
    newTable->createTableImage();

    m_table = newTable;
    return newTable;
}

AbstractBallSet* NormalPoolGame::makeBalls() {
    AbstractBallSet *ballSet = new NormalBallSet();
    QJsonArray balls = m_gameObjects["balls"].toArray();
    NormalBallBuilder bBuilder;

    foreach(const QJsonValue & ball, balls) {
        NormalBall *newBall = bBuilder.buildBall(ball);
        ballSet->addPlayingBall(newBall);
    }

    m_ballSet = ballSet;
    return ballSet;
}
