#include "gameDialog.h"
#include "normalpoolgame.h"

GameDialog::GameDialog(const QString &configFileName, QWidget *parent) : QDialog(parent) {
    QTimer *frameTimer = new QTimer(this);
    connect(frameTimer, SIGNAL(timeout()), this, SLOT(nextFrame()));

    QFile configFile(configFileName);
    ConfigReader config(&configFile);
    poolGame = new NormalPoolGame(config.readFile());
    ballSet = poolGame->makeBalls();
    poolTable = poolGame->makeTable();

    frameTimer->start(32);
}


GameDialog::~GameDialog() {
    delete poolGame;
}

void GameDialog::nextFrame() {
    update();
}

void GameDialog::paintEvent(QPaintEvent *event) {
    QPainter painter(this);
    checkCollision();
    ballSet->moveBalls(poolTable->getFriction());
    poolTable->render(painter);
    ballSet->render(painter);
}


void GameDialog::checkCollision()  {

    for(Ball *ball1 : ballSet->getPlayingBalls()) {
        checkWallCollision(ball1);
        for(int i = ballSet->getPlayingBalls().indexOf(ball1)+1; i<ballSet->getPlayingBalls().length(); i++) {
            Ball *nextBall = ballSet->getPlayingBalls().at(i);
            if(checkBallCollision(ball1, nextBall) == true) {
                calculateCollision(ball1, nextBall);
            }
        }
    }
}


bool GameDialog::checkBallCollision(Ball *ball1, Ball *ball2) {
    QVector2D ball1Vector(ball1->getPosition().x(), ball1->getPosition().y());
    QVector2D ball2Vector(ball2->getPosition().x(), ball2->getPosition().y());
    double distanceToHit = ball1->getRadius() + ball2->getRadius();
    if(ball1Vector.distanceToPoint(ball2Vector) <= distanceToHit) {
        return true;
    }
    else {
        return false;
    }
}

void GameDialog::calculateCollision(Ball *ball1, Ball *ball2) {
    //Properties of two colliding balls,
     //ball A
     QVector2D posA = QVector2D(ball1->getPosition().x(), ball1->getPosition().y());
     QVector2D velA = ball1->getVelocity();
     float massA = ball1->getMass();
     //and ball B
     QVector2D posB = QVector2D(ball2->getPosition().x(), ball2->getPosition().y());
     QVector2D velB = ball2->getVelocity();
     float massB = ball2->getMass();

     //calculate their mass ratio
     float mR = massB / massA;

     //calculate the axis of collision
     QVector2D collisionVector = posB - posA;
     collisionVector.normalize();

    //the proportion of each balls velocity along the axis of collision
     double vA = QVector2D::dotProduct(collisionVector, velA);
     double vB = QVector2D::dotProduct(collisionVector, velB);
     //the balls are moving away from each other so do nothing
     if (vA <= 0 && vB >= 0) {
      return;
     }

     //The velocity of each ball after a collision can be found by solving the quadratic equation
     //given by equating momentum and energy before and after the collision and finding the velocities
     //that satisfy this
     //-(mR+1)x^2 2*(mR*vB+vA)x -((mR-1)*vB^2+2*vA*vB)=0
     //first we find the discriminant
     double a = -(mR + 1);
     double b = 2 * (mR * vB + vA);
     double c = -((mR - 1) * vB * vB + 2 * vA * vB);
     double discriminant = sqrt(b * b - 4 * a * c);
     double root = (-b + discriminant)/(2 * a);
     //only one of the roots is the solution, the other pertains to the current velocities
     if (root - vB < 0.01) {
       root = (-b - discriminant)/(2 * a);
     }


     //The resulting changes in velocity for ball A and B
     QVector2D deltaVB = mR * (vB - root) * collisionVector;
     QVector2D deltaVA = (root - vB) * collisionVector;

     ball1->setVelocity(QVector2D(ball1->getVelocity().x() - deltaVA.x(), ball1->getVelocity().y() - deltaVA.y()));
     ball2->setVelocity(QVector2D(ball2->getVelocity().x() - deltaVB.x(), ball2->getVelocity().y() - deltaVB.y()));
}

void GameDialog::checkWallCollision(Ball *ball) {
    int topOfBall = ball->getPosition().ry() - ball->getRadius();
    int bottomOfBall = ball->getPosition().ry() + ball->getRadius();
    int leftOfBall = ball->getPosition().rx() - ball->getRadius();
    int rightOfBall = ball->getPosition().rx() + ball->getRadius();

    if(topOfBall <= 0 || bottomOfBall >= poolTable->getYSize()) {
        ball->setVelocityY(ball->getVelocity().y() * -1);
        if(topOfBall <= 0) {
            ball->setPosition(QPoint(ball->getPosition().x(), 0 + ball->getRadius()));
        }
        else{
            ball->setPosition(QPoint(ball->getPosition().x(), poolTable->getYSize() - ball->getRadius()));
        }
    }

    if(leftOfBall <= 0 || rightOfBall >= poolTable->getXSize()) {
        ball->setVelocityX(ball->getVelocity().x() * -1);
        if(leftOfBall <= 0) {
            ball->setPosition(QPoint(0 + ball->getRadius() , ball->getPosition().y()));
        }
        else {
            ball->setPosition(QPoint(poolTable->getXSize() - ball->getRadius() , ball->getPosition().y()));
        }
    }
}
