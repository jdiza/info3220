#ifndef ABSTRACTBALLSET_H
#define ABSTRACTBALLSET_H

#include <QList>
#include <ball.h>
#include <QtAlgorithms>
#include <QPainter>

class AbstractBallSet {
public:
    AbstractBallSet() {}

    virtual ~AbstractBallSet() {
        qDeleteAll(m_playingBalls.begin(), m_playingBalls.end());
    }

    QList<Ball *> getPlayingBalls() const {
        return m_playingBalls;
    }

    void addPlayingBall(Ball *newBall) {
        m_playingBalls.append(newBall);
    }

    /**
     * @brief Pure virtual function to be implemented by potentially different types of ball sets for future Assignment stages.
     *
     */
    virtual void render(QPainter &painter) = 0;

    /**
     * @brief Pure virtual function to be implemented by potentially different types of ball sets for future Assignment stages.
     *
     */
    virtual void moveBalls(float friction) = 0;

private:
    /**
     * @brief 1 type of balls in the ballset.
     * Future Assignment stages can include multiple types of balls as part of the game.
     */
    QList<Ball *> m_playingBalls;
};

#endif // ABSTRACTBALLSET_H
