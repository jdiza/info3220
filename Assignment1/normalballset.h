#ifndef NORMALBALLSET_H
#define NORMALBALLSET_H

#include <QColor>

#include "abstractballset.h"

class NormalBallSet : public AbstractBallSet {

public:
    /**
     * @brief Checks if multiple white balls exist at a given time. If not, renders balls as ellipse.
     * @param painter
     */
    void render(QPainter &painter);

    /**
     * @brief Loops through current ballset and move's each ball.
     * @param friction passed to each ball for deceleration calculation.
     */
    void moveBalls(float friction);
};

#endif // NORMALBALLSET_H
