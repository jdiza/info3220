#ifndef GAMEDIALOG_H
#define GAMEDIALOG_H

#include "configreader.h"
#include "abstractgamefactory.h"
#include "ball.h"

#include <QDialog>
#include <QFile>
#include <iostream>
#include <QTimer>
#include <QPainter>
#include <QVector2D>
#include <QPoint>

class GameDialog : public QDialog {
    Q_OBJECT

public:
    /**
     * @brief Constructs game dialog which can work for multiple types of pool games for future Assignment stages.
     * Uses only a single timer of 32ms
     * @param configFileName is specified in main.cpp as preprocessor operator.
     */
    GameDialog(const QString &configFileName, QWidget *parent = 0);
    virtual ~GameDialog();

public slots:
    /**
     * @brief Called every timer cycle.
     */
    void nextFrame();

protected:
    /**
     * @brief Called as a result of nextFrame(). Checks collisions and calculates movements then renders each game object.
     */
    void paintEvent(QPaintEvent *event);

    /**
     * @brief Loops through each ball, passing itself as a param for calling checkWallCollision(Ball *ball) and then with every other ball in the game, checkBallCollision(Ball *ball1, Ball *ball2).
     */
    void checkCollision();

    /**
     * @brief If the parsed ball has passed or is on the edge of the table, it will be repositioned to where it left the table and not exceed the table.
     */
    void checkWallCollision(Ball *ball);

    /**
     * @brief Compares the distance between the centrpoint of each ball with the sum of each ball's radius.
     * @return True if balls are touching/inside eachother, false otherwise.
     */
    bool checkBallCollision(Ball *ball1, Ball *ball2);

    /**
     * @brief Given calculation from Assignment Spec to calculate the change in velocity for 2 balls that have made contact.
     */
    void calculateCollision(Ball *ball1, Ball *ball2);

private:
    /**
     * @brief Interface object for generic pool game.
     */
    AbstractGameFactory *poolGame;

    /**
     * @brief Interface object for generic pool table.
     */
    AbstractTable *poolTable;

    /**
     * @brief Interface object for generic ballset.
     */
    AbstractBallSet *ballSet;
};

#endif // GAMEDIALOG_H
