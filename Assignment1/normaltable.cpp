#include "normaltable.h"

void NormalTable::createTableImage() {
    tableImage = QRect(0,0,this->getXSize(),this->getYSize());
}

void NormalTable::render(QPainter &painter) {
    painter.setBrush(QBrush(this->getColour()));
    painter.drawRect(tableImage);
}
