#ifndef NORMALPOOLGAME_H
#define NORMALPOOLGAME_H

#include "abstractgamefactory.h"
#include "normaltable.h"
#include "normalballset.h"

#include <QJsonObject>
#include <QJsonArray>
#include <iostream>
#include <normalballbuilder.h>

class NormalPoolGame : public AbstractGameFactory {

public:
    NormalPoolGame(const QJsonObject gameObjects);
    /**
     * @brief Deletes table and ballset involved in this instance of a poolgame.
     */
    virtual ~NormalPoolGame();

    /**
     * @brief Decodes table object from the member variable m_gameObjects and accesses each key, setting table components.
     * @return Pointer to a Normal Table object
     */
    AbstractTable* makeTable();

    /**
     * @brief Accesses the array of balls in the json file and calls a builder upon each ball, passing an array value as ball parameters.
     * @return Pointer to a NormalBallSet
     */
    AbstractBallSet* makeBalls();

private:
    const QJsonObject m_gameObjects;
    AbstractTable *m_table;
    AbstractBallSet *m_ballSet;
};

#endif // NORMALPOOLGAME_H
