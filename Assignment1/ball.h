#ifndef BALL_H
#define BALL_H

#include <QString>
#include <QPoint>
#include <QVector2D>
#include <QColor>
#include "abstracttable.h"

class Ball {
public:
    Ball() {}
    virtual ~Ball() {}

    QColor getColour() const{
        return m_colour;
    }
    QPoint getPosition() const {
        return m_coord;
    }
    QVector2D getVelocity() const{
        return m_velocity;
    }
    float getMass() const{
        return m_mass;
    }
    int getRadius() const{
        return m_radius;
    }

    /**
     * @brief Converts a colour as specified as a string to a QColor object.
     */
    void setColour(QString colour) {
        m_colour = QColor(colour);
    }

    /**
     * @brief Set Ball's centrepoint position
     */
    void setPosition(QPoint point) {
        m_coord = point;
    }
    void setVelocity(QVector2D velocity) {
        m_velocity = velocity;
    }
    void setVelocityX(float x) {
        m_velocity.setX(x);
    }
    void setVelocityY(float y) {
        m_velocity.setY(y);
    }
    void setMass(float mass) {
        m_mass = mass;
    }
    void setRadius(int radius) {
        m_radius = radius;
    }

    /**
     * @brief Pure virtual function to allow different types of balls to move differently for future Assignment stages.
     */
    virtual void move(float friction) = 0;

private:
    QColor m_colour;
    QPoint m_coord;
    QVector2D m_velocity;
    float m_mass;
    int m_radius;
};

#endif // BALL_H
