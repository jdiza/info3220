#ifndef CONFIGREADER_H
#define CONFIGREADER_H

#include <QFile>
#include <iostream>
#include <QJsonDocument>
#include <QJsonObject>
#include <QtGlobal>

class ConfigReader {
public:
    ConfigReader(QFile *file);

    /**
     * @brief Reads file that the ConfigReader was created with.
     * @return QJsonObject which contains keys of top level json config file
     */
    const QJsonObject readFile() const;

private:
    QFile *m_file;
};

#endif // CONFIGREADER_H
